package ru.t1.tbobkov.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;

@NoArgsConstructor
public final class FormatUtil {

    private static final double KILOBYTE = 1024;

    private static final double MEGABYTE = KILOBYTE * 1024;

    private static final double GIGABYTE = MEGABYTE * 1024;

    private static final double TERABYTE = GIGABYTE * 1024;

    @NotNull
    private static final String BYTE_NAME = "B";

    @NotNull
    private static final String BYTES_NAME_LONG = "Bytes";

    @NotNull
    private static final String KILOBYTE_NAME = "KB";

    @NotNull
    private static final String MEGABYTE_NAME = "MB";

    @NotNull
    private static final String GIGABYTE_NAME = "GB";

    @NotNull
    private static final String TERABYTE_NAME = "TB";

    @NotNull
    private static final String SEPARATOR = " ";

    @NotNull
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.###");

    @NotNull
    private static String render(final double bytes) {
        return DECIMAL_FORMAT.format(bytes);
    }

    @NotNull
    private static String render(final long bytes, final double size) {
        return render(bytes / size);
    }

    @NotNull
    private static String render(final long bytes, final String name) {
        return render(bytes) + SEPARATOR + name;
    }

    @NotNull
    private static String render(final long bytes, final double size, final String name) {
        return render(bytes / size) + SEPARATOR + name;
    }

    @NotNull
    public static String formatBytes(final long bytes) {
        if ((bytes >= 0) && (bytes < KILOBYTE)) return bytes + SEPARATOR + BYTE_NAME;
        if ((bytes >= KILOBYTE) && (bytes < MEGABYTE)) return render(bytes, KILOBYTE, KILOBYTE_NAME);
        if ((bytes >= MEGABYTE) && (bytes < GIGABYTE)) return render(bytes, MEGABYTE, MEGABYTE_NAME);
        if ((bytes >= GIGABYTE) && (bytes < TERABYTE)) return render(bytes, GIGABYTE, GIGABYTE_NAME);
        if (bytes >= TERABYTE) return render(bytes, TERABYTE, TERABYTE_NAME);
        return render(bytes, BYTES_NAME_LONG);
    }

}