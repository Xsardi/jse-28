package ru.t1.tbobkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.api.repository.IRepository;
import ru.t1.tbobkov.tm.model.AbstractModel;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    private final Map<String, M> models = new LinkedHashMap<>();

    @Override
    public void clear() {
        models.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return new ArrayList<>(models.values());
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return Collections.emptyList();
        return findAll().stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        models.put(model.getId(), model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        models.forEach(model -> this.models.put(model.getId(), model));
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null) return null;
        return models.get(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null) return null;
        return models.get(index);
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) return null;
        models.remove(model.getId());
        return model;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        @Nullable final M model = findOneById(id);
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        return remove(model);
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        collection
                .stream()
                .map(AbstractModel::getId)
                .forEach(models::remove);
    }

}
