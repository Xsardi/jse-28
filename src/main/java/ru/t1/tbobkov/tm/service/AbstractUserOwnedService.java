package ru.t1.tbobkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.api.repository.IUserOwnedRepository;
import ru.t1.tbobkov.tm.api.service.IUserOwnedService;
import ru.t1.tbobkov.tm.enumerated.Sort;
import ru.t1.tbobkov.tm.exception.field.IdEmptyException;
import ru.t1.tbobkov.tm.exception.user.UserIdEmptyException;
import ru.t1.tbobkov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<
        M extends AbstractUserOwnedModel,
        R extends IUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final R repository) {
        super(repository);
    }

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return repository.findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findOneByIndex(userId, index);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(userId, id);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.removeByIndex(userId, index);
    }

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.add(userId, model);
    }

    @Nullable
    @Override
    public M remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.remove(userId, model);
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<M> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, sort.getComparator());
    }

    @Override
    public void removeAll(@NotNull String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeAll(userId);
    }

}
