package ru.t1.tbobkov.tm.exception.field;

public final class TaskIdEmptyException extends AbstractFieldException {

    public TaskIdEmptyException() {
        super("Error! Task Id is empty...");
    }

}
