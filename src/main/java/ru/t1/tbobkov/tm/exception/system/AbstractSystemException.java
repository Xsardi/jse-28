package ru.t1.tbobkov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.exception.AbstractException;

public abstract class AbstractSystemException extends AbstractException {

    public AbstractSystemException() {
    }

    public AbstractSystemException(@NotNull final String message) {
        super(message);
    }

    public AbstractSystemException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractSystemException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractSystemException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
